<?php
	$fileExist = true;
	$fileIndex = 1;
	$folder = 'img/';
	$folderPreviews = 'img/thumbs/';
	if ( file_exists( 'img/thumbs/' ) === false ) {
		mkdir( 'img/thumbs/' );
	}
	while ( $fileExist === true ) {
		if ( file_exists ( $folder.$fileIndex.'.jpg' ) ) {
			$fileName = $folder.$fileIndex.'.jpg';
			$filePreviewName = $folderPreviews.$fileIndex.'.jpg';
			if ( file_exists ($filePreviewName) === false ) {
				$filePreview = fopen( $filePreviewName, 'w' );
				list($width, $height) = getimagesize($fileName);
				$new_width = 200;
				$new_height = $height / $width * 200;
				$image_p = imagecreatetruecolor($new_width, $new_height);
				$image = imagecreatefromjpeg($fileName);
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
				imagejpeg($image_p, $filePreview, 100);
			}
			$files[] = [ $fileName, $filePreviewName, filemtime( $fileName ), filesize( $fileName ) ];
			$fileIndex ++;
		} else {
			$fileExist = false;
		}
	}
	$filesCSV = fopen( 'files.csv', 'w' );
	foreach( $files as $id => $file ) {
		fputcsv( $filesCSV, $file );
	}
	fclose( $filesCSV );
?>