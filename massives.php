<?php
	$animals_twowords = [];
	$continents = [
		'Eurasia' => ['Bison bonasus','Lacerta agilis','Falco tinnunculus'],
		'Africa' => ['Crocodylus niloticus','Panthera leo','Pan troglodytes'],
		'Australia' => ['Thylacinus cynocephalus','Phascolarctos cinereus','Thylacosmilus'],
		'South america' => ['Eunectes murinus','Serrasalmus rhombeus','Brachyteles arachnoides'],
		'North america' => ['Crotalus horridus','Ovibos moschatus','Lynx canadensis']
	];
	foreach($continents as $continent => $animals) {
		foreach($animals as $id => $animal) {
			if (strpos($animal, ' ') !== false ) {
				$animals_twowords[] = [$animal, $continent];
			}
		}
		$animals_fantasyCountries[] = $continent;
	};
	foreach( $animals_twowords as $id => $item ) {
		$animals_firstword[] = [explode( ' ' , $item[0] )[0] , $item[1]];
		$animals_secondword[] = explode( ' ' , $item[0] )[1];
	};
	shuffle( $animals_firstword );
	shuffle( $animals_secondword );
	foreach( $animals_firstword as $id => $item ) {
		$animals_fantasy[] = [$item[0].' '.$animals_secondword[$id], $item[1]];
	};
	$animals_fantasyCountries = array_unique( $animals_fantasyCountries );
	foreach ( $animals_fantasyCountries as $id => $country ) {
		echo "<h1>$country</h1>";
		$animals_fantasyTemp = [];
		foreach( $animals_fantasy as $id => $animal ) {
			if ( $animal[1] === $country ) {
				$animals_fantasyTemp[] = $animal[0];
			}
		}
		echo implode( ',<br>' , $animals_fantasyTemp );
	}
?>